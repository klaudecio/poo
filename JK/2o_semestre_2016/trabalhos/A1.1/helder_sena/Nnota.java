package nnota;
import java.util.Scanner;
import javax.swing.JOptionPane;
// sistema usado para avaliar a nota de aluno
// funciona dessa forma a nota a1 + a nota a2 * 2 e dividida por 3
/**
 *
 * @author heuder rodrigues de sena
 */
public class Nnota {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //o scanner é usado para entrada de dados pelo usuário
        Scanner entrada = new Scanner(System.in);
        double notaA1,notaA2,media;
        
        notaA1 = Double.parseDouble(JOptionPane.showInputDialog("Informe a nota A1"));
        notaA2 = Double.parseDouble(JOptionPane.showInputDialog("Informe a nota A2"));
        
        media = (notaA1 + notaA2*2) / 3;
        
        System.out.println("Amédia é : " +media);
        
        if (media >= 70){
            
            // JOptionPane ´uma janela
             JOptionPane.showMessageDialog(null,"Aluno Aprovado: " + media );
        }else if (media < 70){
            
            // JOptionPane é uma bibliotéca para mostra interface grafica...
            JOptionPane.showMessageDialog(null,"Aluno Reprovado: " + media );            
          
        }
        
        
    }
    
}
